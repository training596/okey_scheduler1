package controllers;

import helpers.FileAccessProcessor;
import models.CalendarModel;
import models.PersonModel;
import models.TaskModel;
import models.TeamModel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.MonthDay;
import java.util.ArrayList;
import java.util.Random;

import static controllers.CalendarController.convertStringToCalendarModel;
import static helpers.FileAccessProcessor.fakeTasks;
import static java.util.Collections.max;

public class TaskController extends Controller{
    public static void createTask(int taskCount) {
        ArrayList<TaskModel> output = new ArrayList<>();
        Random rand = new Random();

        for(int i = 1; i<= taskCount; i++){
            TaskModel t = new TaskModel();
            t.id = i;
            t.name = fakeTasks[rand.nextInt(fakeTasks.length)];
            t.month =rand.nextInt(1,13);
            t.dayOfMonth= rand.nextInt(1,29);
            t.setTaskDay();
            output.add(t);
        }
        saveTasksToFile(output);
    }
    public static void addATask(TaskModel t) {
        ArrayList<TaskModel> tasks = convertStringToTaskModel(FileAccessProcessor.readLinesToList(TaskModel.TASKFILE));
        int currentId = 1;
        //get the max id from that list if any and auto increment it
        if(tasks.size() >=1){
            currentId = max(tasks.stream().map(TaskModel::getId).toList()) + 1;
        }
        //add person to people list
        t.id = currentId;
        tasks.add(t);
        //save the model now to the file
        saveTasksToFile(tasks);

    }
    public static ArrayList<TaskModel> convertStringToTaskModel(ArrayList<String> lines) {
        //id,name,month,day
        ArrayList<TaskModel>output = new ArrayList<>();
        //if list is empty, just return empty list
        if(lines.isEmpty()) return output;
        for (String line:lines) {
            String[] cols = line.split(",");
            TaskModel t = new TaskModel();
            t.id = Integer.parseInt(cols[0]);
            t.name = cols[1];
            t.month = Integer.parseInt(cols[2]);
            t.dayOfMonth = Integer.parseInt(cols[3]);
            t.taskDay = MonthDay.of(t.month,t.dayOfMonth);
            output.add(t);
        }

        return output;
    }
    private static void saveTasksToFile(ArrayList<TaskModel> tasks) {
        //id,name,month,day
        ArrayList<String>lines = new ArrayList<>();
        try {
            FileWriter myWriter = new FileWriter(new File(FileAccessProcessor.dirToFiles+ TaskModel.TASKFILE));
            for (TaskModel task :tasks) {
                lines.add("" + task.id + "," + task.name + "," + task.month + "," + task.dayOfMonth + System.lineSeparator() + "");
            }
            for (String line:lines ) {
                myWriter.write(line);
            }
            myWriter.close();
        }catch (IOException e){
            System.out.println("Record Not Saved! :( OOPs");
        }

    }
    public static ArrayList<TaskModel> getTasksByIds(ArrayList<Integer> TaskIds) {
        ArrayList<TaskModel>tasks = convertStringToTaskModel(FileAccessProcessor.readLinesToList(TaskModel.TASKFILE));
        ArrayList<TaskModel> output = new ArrayList<>();
        for (int Id:TaskIds) {
            output.add(tasks.stream().filter(x -> x.getId() == Id).findFirst().orElse(null));
        }
        return output;
    }
    public static TaskModel getTaskById(int Id) {
        //get task string list
        ArrayList<String> tasks = FileAccessProcessor.readLinesToList(TaskModel.TASKFILE);
        //loop through the list
        for (String task:tasks) {
            //split and check at index 0 if equal to id
            String[] cols = task.split(",");
            if(Integer.parseInt(cols[0])==Id){
                //create new list of task
                ArrayList<String>matchingTasks = new ArrayList<>();
                //add that task to the list
                matchingTasks.add(task);
                //convert list to task.
                return convertStringToTaskModel(matchingTasks).stream().findFirst().orElse(null);
            }
        }
        return null;
    }
    public static void updateTaskModels(TaskModel task) {
        ArrayList<TaskModel>tasks = convertStringToTaskModel(FileAccessProcessor.readLinesToList(TaskModel.TASKFILE));
        TaskModel oldTask = new TaskModel();
        for (TaskModel t:tasks) {
            if(t.id == task.id){
                oldTask = t;
                break;
            }
        }
        tasks.remove(oldTask);
        tasks.add(task);
        saveTasksToFile(tasks);
    }
    public static void deleteTask(TaskModel task) {
        ArrayList<TaskModel>tasks = convertStringToTaskModel(FileAccessProcessor.readLinesToList(TaskModel.TASKFILE));
        TaskModel oldTask = new TaskModel();
        for (TaskModel t:tasks) {
            if(t.id == task.id){
                oldTask = t;
                break;
            }
        }
        tasks.remove(oldTask);
        saveTasksToFile(tasks);
    }
    public static ArrayList<TaskModel> convertToTaskModel(String col) {
        if(col.equals(" ")) {
            return null;
        }
        ArrayList<String>tasks = FileAccessProcessor.readLinesToList(TaskModel.TASKFILE);
        String[] ids = col.split("\\^");
        ArrayList<String> matchingTasks = new ArrayList<>();
        for (String id:ids) {
            for (String task:tasks) {
                String[] cols = task.split(",");
                if(cols[0].equals(id)){
                    matchingTasks.add(task);
                }
            }
        }
        return convertStringToTaskModel(matchingTasks);
    }
    public static ArrayList<TaskModel> getTasksInMonthDay(int month, int day) {
        ArrayList<TaskModel>tasks = convertStringToTaskModel(FileAccessProcessor.readLinesToList(TaskModel.TASKFILE));
        MonthDay targetMonthDay = MonthDay.of(month, day);
        ArrayList<TaskModel> output = new ArrayList<>();
        for (TaskModel t:tasks) {
            if(t.taskDay.compareTo(targetMonthDay) == 0){
                output.add(t);
            }
        }
        return output;
    }
}
