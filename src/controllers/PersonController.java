package controllers;

import helpers.FileAccessProcessor;
import models.PersonModel;
import models.TaskModel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static helpers.FileAccessProcessor.fakeNames;
import static java.util.Collections.max;

public class PersonController {

    public static void createPerson(int peopleCount) {
        ArrayList<PersonModel> output = new ArrayList<>();
        Random rand = new Random();

        for(int i = 1; i<= peopleCount; i++){
            PersonModel p = new PersonModel();
            p.id = i;
            p.name = fakeNames[rand.nextInt(fakeNames.length)];
            output.add(p);
        }
        savePeopleToFile(output);
    }
    public static void addAPerson(PersonModel p)  {
        //load all the people in the excel file
        ArrayList<PersonModel> people = convertStringToPersonModel(FileAccessProcessor.readLinesToList(PersonModel.PERSONFILE));
        int currentId = 1;
        //get the max id from that list if any and auto increment it
        if(people.size() >=1){
            currentId = max(people.stream().map(PersonModel::getId).toList()) + 1;
        }
        //add person to people list
        p.id = currentId;
        people.add(p);
        //save the model now to the file
        savePeopleToFile(people);
    }
    public static ArrayList<PersonModel> convertStringToPersonModel(ArrayList<String> lines) {
        ArrayList<PersonModel>output = new ArrayList<>();
        //if list is empty, just return empty list
        if(lines.isEmpty()) return output;
        for (String line:lines) {
            String[] cols = line.split(",");
            PersonModel p = new PersonModel();
            p.id = Integer.parseInt(cols[0]);
            p.name = cols[1];
            output.add(p);
        }

        return output;
    }
    private static void savePeopleToFile(ArrayList<PersonModel> people) {
        ArrayList<String>lines = new ArrayList<>();
        try {
            FileWriter myWriter = new FileWriter(new File(FileAccessProcessor.dirToFiles + PersonModel.PERSONFILE));
            for (PersonModel person :people) {
                lines.add("" + person.id + "," + person.name + System.lineSeparator() + "");
            }
            for (String line:lines ) {
                myWriter.write(line);
            }
            myWriter.close();
        }catch (IOException e){
            System.out.println("Record Not Saved! :( OOPs");
        }

    }
    public static ArrayList<PersonModel> convertToPersonModel(String col) {
        if(col.equals(" ")) {
            return null;
        }
        ArrayList<String>people = FileAccessProcessor.readLinesToList(PersonModel.PERSONFILE);
        String[] ids = col.split("\\|");
        ArrayList<String> matchingPeople = new ArrayList<>();
        for (String id:ids) {
            for (String person:people) {
                String[] cols = person.split(",");
                if(cols[0].equals(id)){
                    matchingPeople.add(person);
                }
            }
        }
        return convertStringToPersonModel(matchingPeople);

    }
    public static ArrayList<PersonModel> getPeopleByIds(ArrayList<Integer>TeamIds) {
        ArrayList<PersonModel>people = convertStringToPersonModel(FileAccessProcessor.readLinesToList(PersonModel.PERSONFILE));
        ArrayList<PersonModel> output = new ArrayList<>();
        for (int Id:TeamIds) {
            if(Id > people.size()) continue;
            output.add(people.stream().filter(x -> x.getId() == Id).findFirst().orElse(null));
        }
        return output;
    }
    public static PersonModel getPersonById(int Id) {
        //get people string list
        ArrayList<String> people = FileAccessProcessor.readLinesToList(PersonModel.PERSONFILE);
        //loop through the list
        for (String person:people) {
            //split and check at index 0 if equal to id
            String[] cols = person.split(",");
            if(Integer.parseInt(cols[0])==Id){
                //create new list of people
                ArrayList<String>matchingPeople = new ArrayList<>();
                //add that person to the list
                matchingPeople.add(person);
                //convert list to people.
                return convertStringToPersonModel(matchingPeople).stream().findFirst().orElse(null);
            }
        }
        return null;
    }
    public static void updatePersonModels(PersonModel person) {
        ArrayList<PersonModel>people = convertStringToPersonModel(FileAccessProcessor.readLinesToList(PersonModel.PERSONFILE));
        PersonModel oldPerson = new PersonModel();
        for (PersonModel p:people) {
            if(p.id == person.id){
                oldPerson = p;
                break;
            }
        }
        people.remove(oldPerson);
        people.add(person);
        savePeopleToFile(people);
    }
    public static void deletePerson(PersonModel person) {
        ArrayList<PersonModel>people = convertStringToPersonModel(FileAccessProcessor.readLinesToList(PersonModel.PERSONFILE));
        PersonModel oldPerson = new PersonModel();
        for (PersonModel p:people) {
            if(p.id == person.id){
                oldPerson = p;
                break;
            }
        }
        people.remove(oldPerson);
        savePeopleToFile(people);
    }

}
