package controllers;

import models.Model;

import java.util.ArrayList;

public abstract class Controller{
    public static <T extends Model> String convertModelToString(ArrayList<T> models, String delimiter){
        StringBuilder output = new StringBuilder();
        if(models == null) return " ";
        if(models.isEmpty()){
            return " ";
        }
        for (T model:models) {
            output.append(model.getId()).append(delimiter);
        }
        //remove trailing delimiter
        output = new StringBuilder(output.substring(0, output.length() - 1));
        return output.toString();
    }

}
