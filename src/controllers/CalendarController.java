package controllers;

import helpers.FileAccessProcessor;
import models.CalendarModel;
import models.PersonModel;
import models.TaskModel;
import models.TeamModel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Month;
import java.time.MonthDay;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CalendarController extends Controller{
    public static void loadCalendar()  {
        //load all the tasks available
        ArrayList<TaskModel> allTasks = TaskController.convertStringToTaskModel(FileAccessProcessor.readLinesToList(TaskModel.TASKFILE));
        //get the tasks with the same MonthDay to the calendar task list
        Map<MonthDay, List<TaskModel>> tasksGrouped = allTasks.stream().collect(Collectors.groupingBy(x -> x.taskDay));

        //from the list of tasks above, get list of teams with the list of tasks above

        //for every list of month day, create a calendar model and write to file with its corresponding tasks
        //from the loop above, for every list of tasks, check the teams with those tasks and assign them to the
        //list of teams with tasks  on that day.
        ArrayList<CalendarModel> output = new ArrayList<>();
        int count = 1;
        for (Map.Entry<MonthDay,List<TaskModel>>  monthDay:tasksGrouped.entrySet()) {
            CalendarModel thisDay = new CalendarModel();
            thisDay.id = count;
            thisDay.monthDay = monthDay.getKey();
            thisDay.tasks = (ArrayList<TaskModel>) monthDay.getValue();
            thisDay.getTeams();
            count++;

            output.add(thisDay);
        }
        saveCalendarToFile(output);
    }
    private static void saveCalendarToFile(ArrayList<CalendarModel> calendarDays) {
        //id,Month,Day,tkid^tkid^tkid,tmid|tmid|tmid
        ArrayList<String>lines = new ArrayList<>();
        try {
            FileWriter myWriter = new FileWriter(new File(FileAccessProcessor.dirToFiles+ CalendarModel.CALENDARFILE));
            for (CalendarModel cm :calendarDays) {
                lines.add("" + cm.id + "," + cm.monthDay.getMonth() + "," + cm.monthDay.getDayOfMonth() + "," +
                        convertModelToString(cm.tasks,"^") +","+convertModelToString(cm.getTeams(),"|") +
                        System.lineSeparator() + "");
            }
            for (String line:lines ) {
                myWriter.write(line);
            }
            myWriter.close();
        }catch (IOException e){
            System.out.println("Failed To Save Record To File :( OOPs");
        }

    }
    public static ArrayList<CalendarModel> convertStringToCalendarModel(ArrayList<String> lines) {
        ArrayList<CalendarModel>output = new ArrayList<>();
        //if list is empty, just return empty list
        if(lines.isEmpty()) return output;
        for (String line:lines) {
            String[] cols = line.split(",");
            CalendarModel c = new CalendarModel();
            c.id = Integer.parseInt(cols[0]);
            c.monthDay = MonthDay.of(Month.valueOf(cols[1]),Integer.parseInt(cols[2]));
            c.tasks = TaskController.convertToTaskModel(cols[3]);
            c.teams = TeamController.convertToTeamModel(cols[4]);
            output.add(c);
        }

        return output;
    }
    public static ArrayList<TaskModel>getTasksInMonthDay(int month,int day) {
        ArrayList<CalendarModel>calendarDays = convertStringToCalendarModel(FileAccessProcessor.readLinesToList(CalendarModel.CALENDARFILE));
        MonthDay targetMonthDay = MonthDay.of(month, day);
        for (CalendarModel c:calendarDays) {
            if(c.monthDay.compareTo(targetMonthDay) == 0){
                return c.tasks;
            }
        }
        return new ArrayList<>();
    }
    public static ArrayList<TaskModel>getTasksForTeamInMonthDay(TeamModel team,int month, int day) {
        ArrayList<CalendarModel>calendarDays = convertStringToCalendarModel(FileAccessProcessor.readLinesToList(CalendarModel.CALENDARFILE));
        MonthDay targetMonthDay = MonthDay.of(month, day);
        ArrayList<TaskModel> targetTasks = new ArrayList<>();
        for (CalendarModel c:calendarDays) {
            if(c.monthDay.compareTo(targetMonthDay) == 0){
                for (TeamModel tm:c.teams) {
                    if(tm.id == team.id){
                        for (TaskModel tk:team.tasksAssigned) {
                            if(tk.taskDay.compareTo(targetMonthDay) == 0){
                                targetTasks.add(tk);
                            }
                        }
                        return targetTasks;
                    }
                }
            }

        }
        return new ArrayList<>();
    }
    public static ArrayList<TaskModel>getTasksForPersonInMonthDay(PersonModel person, int month, int day) {
        ArrayList<CalendarModel>calendarDays = convertStringToCalendarModel(FileAccessProcessor.readLinesToList(CalendarModel.CALENDARFILE));
        MonthDay targetMonthDay = MonthDay.of(month, day);
        for (CalendarModel c:calendarDays) {
            if(c.monthDay.compareTo(targetMonthDay) == 0){
                if(c.teams != null){
                    for (TeamModel t:c.teams) {
                        if(t.teamMembers != null){
                            for (PersonModel p:t.teamMembers) {
                                if(p.id == person.id){
                                    return c.tasks;
                                }

                            }
                        }

                    }
                }

            }
        }
        return new ArrayList<>();
    }
    public static ArrayList<TaskModel>getTasksForPeopleInMonthDay(int month,int day) {
        ArrayList<CalendarModel>calendarDays = convertStringToCalendarModel(FileAccessProcessor.readLinesToList(CalendarModel.CALENDARFILE));
        MonthDay targetMonthDay = MonthDay.of(month, day);
        for (CalendarModel c:calendarDays) {
            if(c.monthDay.compareTo(targetMonthDay) == 0){
                return c.tasks;
            }
        }
        return new ArrayList<>();
    }

}
//  for (TeamModel t:allTeams) {
//          if(t.tasksAssigned != null){
//          System.out.println(t.id);
//          }
//          }
//        Map<MonthDay, List<models.TaskModel>> map = new HashMap<MonthDay, List<models.TaskModel>>();
//        for (models.TaskModel task : allTasks) {
//            MonthDay key  = task.taskDay;
//            if(map.containsKey(key)){
//                List<models.TaskModel> list = map.get(key);
//                list.add(task);
//
//            }else{
//                List<models.TaskModel> list = new ArrayList<models.TaskModel>();
//                list.add(task);
//                map.put(key, list);
//            }
//
//
//        }

