package controllers;

import helpers.FileAccessProcessor;
import models.PersonModel;
import models.TaskModel;
import models.TeamModel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static helpers.FileAccessProcessor.fakeTeams;
import static java.util.Collections.max;

public class TeamController extends Controller{

    public static void createTeam(int TeamCount) {
        ArrayList<TeamModel> output = new ArrayList<>();
        Random rand = new Random();

        for(int i = 1; i<= TeamCount; i++){
            TeamModel t = new TeamModel();
            t.id = i;
            t.name = fakeTeams[rand.nextInt(fakeTeams.length)];
            output.add(t);
        }
        saveTeamToFile(output);
    }
    public static void addATeam(TeamModel t) {
        //load all the people in the excel file
        ArrayList<TeamModel> teams = convertStringToTeamModel(FileAccessProcessor.readLinesToList(TeamModel.TEAMFILE));
        int currentId = 1;
        //get the max id from that list if any and auto increment it
        if(teams.size() >=1){
            currentId = max(teams.stream().map(TeamModel::getId).toList()) + 1;
        }
        //add person to people list
        t.id = currentId;
        teams.add(t);
        //save the model now to the file
        saveTeamToFile(teams);
    }
    public static ArrayList<TeamModel> convertStringToTeamModel(ArrayList<String> lines) {
        ArrayList<TeamModel> output = new ArrayList<>();
        if(lines.isEmpty()) return output;
        for (String line:lines ) {
            String[] cols = line.split(",");
            //System.out.println(cols.length);
            TeamModel t = new TeamModel();
            t.id = Integer.parseInt(cols[0]);
            t.name = cols[1];
            //on start both cols[2] and cols[3] are null
            t.teamMembers = PersonController.convertToPersonModel(cols[2]);
            t.tasksAssigned = TaskController.convertToTaskModel(cols[3]);
            output.add(t);
        }
        return output;
    }
    private static void saveTeamToFile(ArrayList<TeamModel> teams) {
        ArrayList<String>lines = new ArrayList<>();
        try {
            FileWriter myWriter = new FileWriter(new File(FileAccessProcessor.dirToFiles + TeamModel.TEAMFILE));
            for (TeamModel t :teams) {
                lines.add("" + t.id + "," + t.name + ","+ convertModelToString(t.teamMembers,"|") +","
                        +convertModelToString(t.tasksAssigned,"^") + System.lineSeparator() + "");
            }
            for (String line:lines ) {
                myWriter.write(line);
            }
            myWriter.close();
        }catch (IOException e){
            System.out.println("Record Not Saved To File :( OOps");
        }

    }
    public static TeamModel getTeamById(int Id) {
        //get team string list
        ArrayList<String> teams = FileAccessProcessor.readLinesToList(TeamModel.TEAMFILE);
        //loop through the list
        for (String team:teams) {
            //split and check at index 0 if equal to id
            String[] cols = team.split(",");
            if(Integer.parseInt(cols[0])==Id){
                //create new list of teams
                ArrayList<String>matchingTeams = new ArrayList<>();
                //add that team to the list
                matchingTeams.add(team);
                //convert list to teammodel.
                return convertStringToTeamModel(matchingTeams).stream().findFirst().orElse(null);
            }
        }
        return null;

    }
    public static void updateTeamModels(TeamModel team) {
        ArrayList<TeamModel>teams = convertStringToTeamModel(FileAccessProcessor.readLinesToList(TeamModel.TEAMFILE));
        TeamModel oldTeam = new TeamModel();
        for (TeamModel t:teams) {
            if(t.id == team.id){
                oldTeam = t;
                break;
            }
        }
        teams.remove(oldTeam);
        teams.add(team);
        saveTeamToFile(teams);
    }
    public static ArrayList<TeamModel> convertToTeamModel(String col) {
        if(col.equals(" ")) {
            return null;
        }
        ArrayList<String>teams = FileAccessProcessor.readLinesToList(TeamModel.TEAMFILE);
        String[] ids = col.split("\\|");
        ArrayList<String> matchingTasks = new ArrayList<>();
        for (String id:ids) {
            for (String team:teams) {
                String[] cols = team.split(",");
                if(cols[0].equals(id)){
                    matchingTasks.add(team);
                }
            }
        }
        return convertStringToTeamModel(matchingTasks);
    }
    public static ArrayList<TeamModel>getTeamsHavingTask(TaskModel task) {
        ArrayList<TeamModel>teams = convertStringToTeamModel(FileAccessProcessor.readLinesToList(TeamModel.TEAMFILE));
        ArrayList<TeamModel> output = new ArrayList<>();
        for (TeamModel t:teams) {
           if(t.tasksAssigned != null){
               for (TaskModel tk:t.tasksAssigned) {
                   if(tk.id == task.id){
                       output.add(t);
                   }
               }
           }
        }
        return output;
    }
    public static ArrayList<TaskModel>getTasksForPerson(PersonModel person) {
        ArrayList<TeamModel>teams = convertStringToTeamModel(FileAccessProcessor.readLinesToList(TeamModel.TEAMFILE));
        for (TeamModel t:teams) {
            if(t.teamMembers != null){
                for (PersonModel p:t.teamMembers) {
                    if(p.id == person.id){
                        return t.tasksAssigned;
                    }
                }
            }
        }
        return new ArrayList<>();
    }
    public static void deleteTeam(TeamModel team) {
        ArrayList<TeamModel>teams = convertStringToTeamModel(FileAccessProcessor.readLinesToList(TeamModel.TEAMFILE));
        TeamModel oldTeam = new TeamModel();
        for (TeamModel t:teams) {
            if(t.id == team.id){
                oldTeam = t;
                break;
            }
        }
        teams.remove(oldTeam);
        saveTeamToFile(teams);
    }
}
