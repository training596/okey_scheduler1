package helpers;

import models.*;
import models.CalendarModel;

import java.io.*;
import java.time.MonthDay;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.max;

public class FileAccessProcessor {
    public static final String dirToFiles = "/home/jayced1loved/Documents/";
    public static final String [] modelFiles = {"TaskModel.csv","PersonModel.csv","TeamModel.csv","CalendarModel.csv"};
    public static String[] fakeNames = {"Liam","Noah","Oliver","Elijah","Olivia","Emma","Lucas","Mia","Henry", "Evelyn",
                                         "Theodore", "Harper","William","Benjamin","Isabella", "Sophia","Charlotte","Amelia",
                                            "James","Ava"};
    public static String[] fakeTeams = {"Red Demons" ,"The Slaying Ninjas","Head Hunters","Crashing Crusaders",
                                        "The Warriors","The Hunters and Gatherers","Wolf Pack","Fiery Dragons",
                                        "The Pistols","The Stoney Gang","The Amazonians","The Shackled Sharks"};
    public static String[] fakeTasks = {"Hire and train personnel" ,"Maintain records of employee work history and tax information",
                                        "Administer employee benefits","Address employee issues and complaints","Take disciplinary action in case of poor employee performance",
                                        "Keep inventory records tracking what you have on hand","Order supplies and materials as needed","Organize stock so you know when to reorder",
                                        "Receive orders and set production numbers","Monitor and improve workflow","Maintain and repair equipment","Package and store finished product",
                                        "Work with customers through each phase of your sales process","Receive and respond to customer feedback","Take care of customer needs and special requests",
                                        "Create a marketing budget","Manage company branding","Oversee advertising copy and venues",
                                        "Align each aspect of your company's operations to create an image that reinforces your brand"};

    //Creating all files needed
    public static void createFiles() {
        for (String filename:modelFiles) {
            File file = new File(dirToFiles + filename);
            try {
                if (file.createNewFile()){
                    System.out.println("File has been created");
                }else {
                    System.out.println("File has already been created");
                }
            } catch (IOException e) {
               System.out.println("Something went wrong! No file was created");
            }
        }
    }
    public static ArrayList<String> readLinesToList(String filename) {
        ArrayList<String>lines = new ArrayList<>();
        try {
            BufferedReader output = new BufferedReader(new FileReader(dirToFiles+ filename));
            String line;
            while ((line = output.readLine()) != null){

                lines.add(line);
            }
            return lines;
        }catch (IOException e){
            System.out.println("Failed To Read File!");
            return lines;
        }

    }

}
