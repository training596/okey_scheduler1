package models;

import controllers.TeamController;
import helpers.FileAccessProcessor;

import java.io.IOException;
import java.time.MonthDay;
import java.util.ArrayList;

public class CalendarModel {

    public int id;
    public MonthDay monthDay;
    public static final String CALENDARFILE = "CalendarModel.csv";
    public ArrayList<TaskModel>tasks = new ArrayList<>();
    public ArrayList<TeamModel>teams = new ArrayList<>();
    public ArrayList<TeamModel> getTeams() {
        ArrayList<TeamModel> allTeams = TeamController.convertStringToTeamModel(FileAccessProcessor.readLinesToList(TeamModel.TEAMFILE));

        ArrayList<TeamModel> output = new ArrayList<>();
        for (TeamModel tms:allTeams) {
            for (TaskModel tk:tasks) {
                if(tms.tasksAssigned == null) continue;
                for(TaskModel tall :tms.tasksAssigned){
                    if(tk.id == tall.id){
                        output.add(tms);
                    }
                }

            }
        }
        return output;
    }
}
//        allTeams.forEach(teamModel -> {
//            if (teamModel.tasksAssigned.stream().anyMatch(task -> this.tasks.contains(task))) {
//                this.teams.add(teamModel);
//            }
//        });
//        return teams;
