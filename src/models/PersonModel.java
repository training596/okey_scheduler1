package models;

public class PersonModel extends Model {
    public static final String PERSONFILE = "PersonModel.csv";

    public int id;
    public String name;
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }

//    public models.TeamModel team = new models.TeamModel();
}