package models;

import java.time.MonthDay;

public class TaskModel extends Model {

    public int id;
    public String name;
    public static final String TASKFILE = "TaskModel.csv";
    public int month;
    public int dayOfMonth;

    public MonthDay taskDay = MonthDay.of(1,1); //default value

    public void setTaskDay() {
        this.taskDay = MonthDay.of(this.month,this.dayOfMonth);
    }

    public TaskModel(){

    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
