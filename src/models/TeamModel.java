package models;

import java.util.ArrayList;

public class TeamModel extends Model {
    public int id;
    public String name;
    public static final String TEAMFILE = "TeamModel.csv";
    public ArrayList<PersonModel>teamMembers = new ArrayList<>();

    public int teamcount = getTasksAssigned() == null?0:getTasksAssigned().size();
    public ArrayList<TaskModel> tasksAssigned = new ArrayList<>();//tasks that belong to a team

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<PersonModel> getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(ArrayList<PersonModel> teamMembers) {
        this.teamMembers = teamMembers;
    }

    public ArrayList<TaskModel> getTasksAssigned() {
        return tasksAssigned;
    }

    public void setTasksAssigned(ArrayList<TaskModel> tasksAssigned) {
        this.tasksAssigned = tasksAssigned;
    }
}
