package views;

import helpers.FileAccessProcessor;

import java.util.Scanner;

public class ProjectSetUp {
    public static void SetUpProject() {
        Scanner in = new Scanner(System.in);

        FileAccessProcessor.createFiles();
        System.out.println("WELCOME TO THE SCHEDULER APPLICATION");
        System.out.println();
        System.out.println("Press 1 To Create People ");
        System.out.println("Press 2 To Create Tasks ");
        System.out.println("Press 3 To Create Teams ");
        int SetUpId = in.nextInt();
        while (!in.hasNextInt()){
            System.out.println("Press Either 1, 2 Or 3 To Continue");
            SetUpId = in.nextInt();
        }
        if(SetUpId == 1){
            ConsoleInputDisplay.CreatePeople();
            System.out.println("Press 2 To Continue");
            SetUpId = in.nextInt();
            while (SetUpId != 2){
                System.out.println("Press 2 To Continue");
                SetUpId = in.nextInt();
            }
            ConsoleInputDisplay.CreateTasks();
            ConsoleInputDisplay.CreateTeams();

            System.out.println("Press 1,2 Or 3 To Add A CUSTOM PERSON,TASK Or TEAM Respectively");
            SetUpId = in.nextInt();
            if(SetUpId == 1){
                ConsoleInputDisplay.AddAnotherPerson();
                System.out.println("Press 2 To Add Another Custom Task Or Any Other Number To Skip");
                SetUpId = in.nextInt();
                if(SetUpId == 2){
                    ConsoleInputDisplay.AddAnotherTask();
                }//else {
                    System.out.println("Press 3 To Add Another Custom Team Any Other Number To Skip");
                    SetUpId = in.nextInt();
                    if(SetUpId == 3){
                        ConsoleInputDisplay.AddAnotherTeam();
                    }
                //}
                boolean runs = true;
                while (runs){
                    System.out.println("Press 1 To Update Team");
                    System.out.println("Press 2 To Delete Team");
                    System.out.println("Press 3 To Update Task");
                    System.out.println("Press 4 To Delete Task");
                    System.out.println("Press 5 To Update Person");
                    System.out.println("Press 6 To Delete Person");
                    System.out.println("Press 0 To Skip");
                    int Choice = in.nextInt();

                    switch (Choice) {
                        case 1 -> ConsoleInputDisplay.UpdateTeam();
                        case 2 -> ConsoleInputDisplay.DeleteTeam();
                        case 3 -> ConsoleInputDisplay.UpdateTask();
                        case 4 -> ConsoleInputDisplay.DeleteTask();
                        case 5 -> ConsoleInputDisplay.UpdatePerson();
                        case 6 -> ConsoleInputDisplay.DeletePerson();
                        default -> runs = false;
                    }

                }
                System.out.println("How Many Teams Do You Want To Attach People And Tasks?");
                    int TeamCount = in.nextInt();
                while (TeamCount> 0){
                    ConsoleInputDisplay.AssignPeopleToTeams();
                    ConsoleInputDisplay.AssignTasksToTeams();
                    TeamCount --;
                }

                //TODO populate the calendar and Begin Reporting
                ConsoleInputDisplay.PopulateCalendar();
                //TODO REPORTING HERE
                boolean run = true;
                while(run){
                    System.out.println("Press 1 To Get Tasks In Given Date");
                    System.out.println("Press 2 To Get Tasks In Given Date For A Given Team");
                    System.out.println("Press 3 To Get Tasks For A Given Person");
                    System.out.println("Press 4 To Get Task In A Given Date For A Given Person");
                    System.out.println("Press 5 To Get A Team And Person In Given Task");
                    int ReportNumber = in.nextInt();

                    switch (ReportNumber) {
                        case 1 -> ConsoleOutputDisplay.GetTasksInDateX();
                        case 2 -> ConsoleOutputDisplay.GetTasksInDateXForTeamX();
                        case 3 -> ConsoleOutputDisplay.GetTasksForPersonX();
                        case 4 -> ConsoleOutputDisplay.GetTaskInDateXForPersonX();
                        default -> ConsoleOutputDisplay.GetTeamAndPeopleInTaskX();
                    }
                    System.out.println("Press 1 To Continue Or 0 To Quit");
                    ReportNumber = in.nextInt();
                    while (!in.hasNextInt()){
                        System.out.println("Press 1 To Continue Or 0 To Quit");
                        ReportNumber = in.nextInt();
                    }
                    run = ReportNumber ==1;

                }
            } else if (SetUpId == 2) {
                ConsoleInputDisplay.AddAnotherTask();
                System.out.println("Press 1 To Add Another Custom Person");
                SetUpId = in.nextInt();
                if(SetUpId == 1){
                    ConsoleInputDisplay.AddAnotherPerson();
                }//else {
                    System.out.println("Press 3 To Add Another Custom Team");
                    SetUpId = in.nextInt();
                    if(SetUpId == 3){
                        ConsoleInputDisplay.AddAnotherTeam();
                    }
               // }
                boolean runs = true;
                while (runs){
                    System.out.println("Press 1 To Update Team");
                    System.out.println("Press 2 To Delete Team");
                    System.out.println("Press 3 To Update Task");
                    System.out.println("Press 4 To Delete Task");
                    System.out.println("Press 5 To Update Person");
                    System.out.println("Press 6 To Delete Person");
                    System.out.println("Press 0 To Skip");
                    int Choice = in.nextInt();

                    switch (Choice) {
                        case 1 -> ConsoleInputDisplay.UpdateTeam();
                        case 2 -> ConsoleInputDisplay.DeleteTeam();
                        case 3 -> ConsoleInputDisplay.UpdateTask();
                        case 4 -> ConsoleInputDisplay.DeleteTask();
                        case 5 -> ConsoleInputDisplay.UpdatePerson();
                        case 6 -> ConsoleInputDisplay.DeletePerson();
                        default -> runs = false;
                    }

                }
                //TODO setup done, attaching begins here 2
                System.out.println("How Many Teams Do You Want To Attach People And Tasks?");
                int TeamCount = in.nextInt();
                while (TeamCount> 0){
                    ConsoleInputDisplay.AssignPeopleToTeams();
                    ConsoleInputDisplay.AssignTasksToTeams();
                    TeamCount --;
                }
                //TODO populate the calendar and Begin Reporting
                ConsoleInputDisplay.PopulateCalendar();
                //TODO REPORTING HERE
                boolean run = true;
                while(run){
                    System.out.println("Press 1 To Get Tasks In Given Date");
                    System.out.println("Press 2 To Get Tasks In Given Date For A Given Team");
                    System.out.println("Press 3 To Get Tasks For A Given Person");
                    System.out.println("Press 4 To Get Task In A Given Date For A Given Person");
                    System.out.println("Press 5 To Get A Team And Person In Given Task");
                    int ReportNumber = in.nextInt();

                    switch (ReportNumber) {
                        case 1 -> ConsoleOutputDisplay.GetTasksInDateX();
                        case 2 -> ConsoleOutputDisplay.GetTasksInDateXForTeamX();
                        case 3 -> ConsoleOutputDisplay.GetTasksForPersonX();
                        case 4 -> ConsoleOutputDisplay.GetTaskInDateXForPersonX();
                        default -> ConsoleOutputDisplay.GetTeamAndPeopleInTaskX();
                    }
                    System.out.println("Press 1 To Continue Or 0 To Quit");
                    ReportNumber = in.nextInt();

                    while (!in.hasNextInt()){
                        System.out.println("Press 1 To Continue Or 0 To Quit");
                        ReportNumber = in.nextInt();
                    }
                    run = ReportNumber == 1;

                }
            } else if (SetUpId == 3) {
                ConsoleInputDisplay.AddAnotherTeam();
                System.out.println("Press 1 To Add Another Custom Person");
                SetUpId = in.nextInt();
                if(SetUpId == 1){
                    ConsoleInputDisplay.AddAnotherPerson();
                }//else {
                    System.out.println("Press 2 To Add Another Custom Task");
                    SetUpId = in.nextInt();
                    if(SetUpId == 2){
                        ConsoleInputDisplay.AddAnotherTask();
                    }
               // }
                boolean runs = true;
                while (runs){
                    System.out.println("Press 1 To Update Team");
                    System.out.println("Press 2 To Delete Team");
                    System.out.println("Press 3 To Update Task");
                    System.out.println("Press 4 To Delete Task");
                    System.out.println("Press 5 To Update Person");
                    System.out.println("Press 6 To Delete Person");
                    System.out.println("Press 0 To Skip");
                    int Choice = in.nextInt();

                    switch (Choice) {
                        case 1 -> ConsoleInputDisplay.UpdateTeam();
                        case 2 -> ConsoleInputDisplay.DeleteTeam();
                        case 3 -> ConsoleInputDisplay.UpdateTask();
                        case 4 -> ConsoleInputDisplay.DeleteTask();
                        case 5 -> ConsoleInputDisplay.UpdatePerson();
                        case 6 -> ConsoleInputDisplay.DeletePerson();
                        default -> runs = false;
                    }

                }
                //TODO setup done, attaching begins here 3
                System.out.println("How Many Teams Do You Want To Attach People And Tasks?");
                int TeamCount = in.nextInt();
                while (TeamCount> 0){
                    ConsoleInputDisplay.AssignPeopleToTeams();
                    ConsoleInputDisplay.AssignTasksToTeams();
                    TeamCount --;
                }
                //TODO populate the calendar and Begin Reporting
                ConsoleInputDisplay.PopulateCalendar();
                //TODO REPORTING HERE
                boolean run = true;
                while(run){
                    System.out.println("Press 1 To Get Tasks In Given Date");
                    System.out.println("Press 2 To Get Tasks In Given Date For A Given Team");
                    System.out.println("Press 3 To Get Tasks For A Given Person");
                    System.out.println("Press 4 To Get Task In A Given Date For A Given Person");
                    System.out.println("Press 5 To Get A Team And Person In Given Task");
                    int ReportNumber = in.nextInt();

                    switch (ReportNumber) {
                        case 1 -> ConsoleOutputDisplay.GetTasksInDateX();
                        case 2 -> ConsoleOutputDisplay.GetTasksInDateXForTeamX();
                        case 3 -> ConsoleOutputDisplay.GetTasksForPersonX();
                        case 4 -> ConsoleOutputDisplay.GetTaskInDateXForPersonX();
                        default -> ConsoleOutputDisplay.GetTeamAndPeopleInTaskX();
                    }
                    System.out.println("Press 1 To Continue Or 0 To Quit");
                    ReportNumber = in.nextInt();
                    while (!in.hasNextInt()){
                        System.out.println("Press 1 To Continue Or 0 To Quit");
                        ReportNumber = in.nextInt();
                    }
                    run = ReportNumber == 1;

                }
            }
        } else if (SetUpId == 2) {
            ConsoleInputDisplay.CreateTasks();
            System.out.println("Press 1 Continue");
            SetUpId = in.nextInt();
            while (SetUpId != 1){
                System.out.println("Incorrect Value, Press 1 Continue");
                SetUpId = in.nextInt();
            }
            ConsoleInputDisplay.CreatePeople();
            ConsoleInputDisplay.CreateTeams();
            System.out.println("Press 1,2 Or 3 To Add A CUSTOM PERSON,TASK Or TEAM Respectively");
            SetUpId = in.nextInt();
            if(SetUpId == 1){
                ConsoleInputDisplay.AddAnotherPerson();
                System.out.println("Press 2 To Add Another Custom Task Or Any Other Number To Skip");
                SetUpId = in.nextInt();
                if(SetUpId == 2){
                    ConsoleInputDisplay.AddAnotherTask();
                }//else {
                System.out.println("Press 3 To Add Another Custom Team Any Other Number To Skip");
                SetUpId = in.nextInt();
                if(SetUpId == 3){
                    ConsoleInputDisplay.AddAnotherTeam();
                }
                //}
                boolean runs = true;
                while (runs){
                    System.out.println("Press 1 To Update Team");
                    System.out.println("Press 2 To Delete Team");
                    System.out.println("Press 3 To Update Task");
                    System.out.println("Press 4 To Delete Task");
                    System.out.println("Press 5 To Update Person");
                    System.out.println("Press 6 To Delete Person");
                    System.out.println("Press 0 To Skip");
                    int Choice = in.nextInt();

                    switch (Choice) {
                        case 1 -> ConsoleInputDisplay.UpdateTeam();
                        case 2 -> ConsoleInputDisplay.DeleteTeam();
                        case 3 -> ConsoleInputDisplay.UpdateTask();
                        case 4 -> ConsoleInputDisplay.DeleteTask();
                        case 5 -> ConsoleInputDisplay.UpdatePerson();
                        case 6 -> ConsoleInputDisplay.DeletePerson();
                        default -> runs = false;
                    }

                }
                System.out.println("How Many Teams Do You Want To Attach People And Tasks?");
                int TeamCount = in.nextInt();
                while (TeamCount> 0){
                    ConsoleInputDisplay.AssignPeopleToTeams();
                    ConsoleInputDisplay.AssignTasksToTeams();
                    TeamCount --;
                }

                //TODO populate the calendar and Begin Reporting
                ConsoleInputDisplay.PopulateCalendar();
                //TODO REPORTING HERE
                boolean run = true;
                while(run){
                    System.out.println("Press 1 To Get Tasks In Given Date");
                    System.out.println("Press 2 To Get Tasks In Given Date For A Given Team");
                    System.out.println("Press 3 To Get Tasks For A Given Person");
                    System.out.println("Press 4 To Get Task In A Given Date For A Given Person");
                    System.out.println("Press 5 To Get A Team And Person In Given Task");
                    int ReportNumber = in.nextInt();

                    switch (ReportNumber) {
                        case 1 -> ConsoleOutputDisplay.GetTasksInDateX();
                        case 2 -> ConsoleOutputDisplay.GetTasksInDateXForTeamX();
                        case 3 -> ConsoleOutputDisplay.GetTasksForPersonX();
                        case 4 -> ConsoleOutputDisplay.GetTaskInDateXForPersonX();
                        default -> ConsoleOutputDisplay.GetTeamAndPeopleInTaskX();
                    }
                    System.out.println("Press 1 To Continue Or 0 To Quit");
                    ReportNumber = in.nextInt();
                    while (!in.hasNextInt()){
                        System.out.println("Press 1 To Continue Or 0 To Quit");
                        ReportNumber = in.nextInt();
                    }
                    run = ReportNumber ==1;

                }
            } else if (SetUpId == 2) {
                ConsoleInputDisplay.AddAnotherTask();
                System.out.println("Press 1 To Add Another Custom Person");
                SetUpId = in.nextInt();
                if(SetUpId == 1){
                    ConsoleInputDisplay.AddAnotherPerson();
                }//else {
                System.out.println("Press 3 To Add Another Custom Team");
                SetUpId = in.nextInt();
                if(SetUpId == 3){
                    ConsoleInputDisplay.AddAnotherTeam();
                }
                // }
                boolean runs = true;
                while (runs){
                    System.out.println("Press 1 To Update Team");
                    System.out.println("Press 2 To Delete Team");
                    System.out.println("Press 3 To Update Task");
                    System.out.println("Press 4 To Delete Task");
                    System.out.println("Press 5 To Update Person");
                    System.out.println("Press 6 To Delete Person");
                    System.out.println("Press 0 To Skip");
                    int Choice = in.nextInt();

                    switch (Choice) {
                        case 1 -> ConsoleInputDisplay.UpdateTeam();
                        case 2 -> ConsoleInputDisplay.DeleteTeam();
                        case 3 -> ConsoleInputDisplay.UpdateTask();
                        case 4 -> ConsoleInputDisplay.DeleteTask();
                        case 5 -> ConsoleInputDisplay.UpdatePerson();
                        case 6 -> ConsoleInputDisplay.DeletePerson();
                        default -> runs = false;
                    }

                }
                //TODO setup done, attaching begins here 2
                System.out.println("How Many Teams Do You Want To Attach People And Tasks?");
                int TeamCount = in.nextInt();
                while (TeamCount> 0){
                    ConsoleInputDisplay.AssignPeopleToTeams();
                    ConsoleInputDisplay.AssignTasksToTeams();
                    TeamCount --;
                }
                //TODO populate the calendar and Begin Reporting
                ConsoleInputDisplay.PopulateCalendar();
                //TODO REPORTING HERE
                boolean run = true;
                while(run){
                    System.out.println("Press 1 To Get Tasks In Given Date");
                    System.out.println("Press 2 To Get Tasks In Given Date For A Given Team");
                    System.out.println("Press 3 To Get Tasks For A Given Person");
                    System.out.println("Press 4 To Get Task In A Given Date For A Given Person");
                    System.out.println("Press 5 To Get A Team And Person In Given Task");
                    int ReportNumber = in.nextInt();

                    switch (ReportNumber) {
                        case 1 -> ConsoleOutputDisplay.GetTasksInDateX();
                        case 2 -> ConsoleOutputDisplay.GetTasksInDateXForTeamX();
                        case 3 -> ConsoleOutputDisplay.GetTasksForPersonX();
                        case 4 -> ConsoleOutputDisplay.GetTaskInDateXForPersonX();
                        default -> ConsoleOutputDisplay.GetTeamAndPeopleInTaskX();
                    }
                    System.out.println("Press 1 To Continue Or 0 To Quit");
                    ReportNumber = in.nextInt();
                    while (!in.hasNextInt()){
                        System.out.println("Press 1 To Continue Or 0 To Quit");
                        ReportNumber = in.nextInt();
                    }
                    run = ReportNumber == 1;

                }
            } else if (SetUpId == 3) {
                ConsoleInputDisplay.AddAnotherTeam();
                System.out.println("Press 1 To Add Another Custom Person");
                SetUpId = in.nextInt();
                if(SetUpId == 1){
                    ConsoleInputDisplay.AddAnotherPerson();
                }//else {
                System.out.println("Press 2 To Add Another Custom Task");
                SetUpId = in.nextInt();
                if(SetUpId == 2){
                    ConsoleInputDisplay.AddAnotherTask();
                }
                // }
                boolean runs = true;
                while (runs){
                    System.out.println("Press 1 To Update Team");
                    System.out.println("Press 2 To Delete Team");
                    System.out.println("Press 3 To Update Task");
                    System.out.println("Press 4 To Delete Task");
                    System.out.println("Press 5 To Update Person");
                    System.out.println("Press 6 To Delete Person");
                    System.out.println("Press 0 To Skip");
                    int Choice = in.nextInt();

                    switch (Choice) {
                        case 1 -> ConsoleInputDisplay.UpdateTeam();
                        case 2 -> ConsoleInputDisplay.DeleteTeam();
                        case 3 -> ConsoleInputDisplay.UpdateTask();
                        case 4 -> ConsoleInputDisplay.DeleteTask();
                        case 5 -> ConsoleInputDisplay.UpdatePerson();
                        case 6 -> ConsoleInputDisplay.DeletePerson();
                        default -> runs = false;
                    }

                }
                //TODO setup done, attaching begins here 3
                System.out.println("How Many Teams Do You Want To Attach People And Tasks?");
                int TeamCount = in.nextInt();
                while (TeamCount> 0){
                    ConsoleInputDisplay.AssignPeopleToTeams();
                    ConsoleInputDisplay.AssignTasksToTeams();
                    TeamCount --;
                }
                //TODO populate the calendar and Begin Reporting
                ConsoleInputDisplay.PopulateCalendar();
                //TODO REPORTING HERE
                boolean run = true;
                while(run){
                    System.out.println("Press 1 To Get Tasks In Given Date");
                    System.out.println("Press 2 To Get Tasks In Given Date For A Given Team");
                    System.out.println("Press 3 To Get Tasks For A Given Person");
                    System.out.println("Press 4 To Get Task In A Given Date For A Given Person");
                    System.out.println("Press 5 To Get A Team And Person In Given Task");
                    int ReportNumber = in.nextInt();

                    switch (ReportNumber) {
                        case 1 -> ConsoleOutputDisplay.GetTasksInDateX();
                        case 2 -> ConsoleOutputDisplay.GetTasksInDateXForTeamX();
                        case 3 -> ConsoleOutputDisplay.GetTasksForPersonX();
                        case 4 -> ConsoleOutputDisplay.GetTaskInDateXForPersonX();
                        default -> ConsoleOutputDisplay.GetTeamAndPeopleInTaskX();
                    }
                    System.out.println("Press 1 To Continue Or 0 To Quit");
                    ReportNumber = in.nextInt();
                    while (!in.hasNextInt()){
                        System.out.println("Press 1 To Continue Or 0 To Quit");
                        ReportNumber = in.nextInt();
                    }
                    run = ReportNumber == 1;

                }
            }
        } else if (SetUpId == 3) {
            ConsoleInputDisplay.CreateTeams();
            System.out.println("Press 1 Continue");
            SetUpId = in.nextInt();
            while (SetUpId != 1){
                System.out.println("Incorrect Value, Press 1 Continue");
                SetUpId = in.nextInt();
            }
            ConsoleInputDisplay.CreatePeople();
            ConsoleInputDisplay.CreateTasks();
            System.out.println("Press 1,2 Or 3 To Add A CUSTOM PERSON,TASK Or TEAM Respectively");
            SetUpId = in.nextInt();
            if(SetUpId == 1){
                ConsoleInputDisplay.AddAnotherPerson();
                System.out.println("Press 2 To Add Another Custom Task Or Any Other Number To Skip");
                SetUpId = in.nextInt();
                if(SetUpId == 2){
                    ConsoleInputDisplay.AddAnotherTask();
                }//else {
                System.out.println("Press 3 To Add Another Custom Team Any Other Number To Skip");
                SetUpId = in.nextInt();
                if(SetUpId == 3){
                    ConsoleInputDisplay.AddAnotherTeam();
                }
                //}
                boolean runs = true;
                while (runs){
                    System.out.println("Press 1 To Update Team");
                    System.out.println("Press 2 To Delete Team");
                    System.out.println("Press 3 To Update Task");
                    System.out.println("Press 4 To Delete Task");
                    System.out.println("Press 5 To Update Person");
                    System.out.println("Press 6 To Delete Person");
                    System.out.println("Press 0 To Skip");
                    int Choice = in.nextInt();

                    switch (Choice) {
                        case 1 -> ConsoleInputDisplay.UpdateTeam();
                        case 2 -> ConsoleInputDisplay.DeleteTeam();
                        case 3 -> ConsoleInputDisplay.UpdateTask();
                        case 4 -> ConsoleInputDisplay.DeleteTask();
                        case 5 -> ConsoleInputDisplay.UpdatePerson();
                        case 6 -> ConsoleInputDisplay.DeletePerson();
                        default -> runs = false;
                    }

                }
                System.out.println("How Many Teams Do You Want To Attach People And Tasks?");
                int TeamCount = in.nextInt();
                while (TeamCount> 0){
                    ConsoleInputDisplay.AssignPeopleToTeams();
                    ConsoleInputDisplay.AssignTasksToTeams();
                    TeamCount --;
                }

                //TODO populate the calendar and Begin Reporting
                ConsoleInputDisplay.PopulateCalendar();
                //TODO REPORTING HERE
                boolean run = true;
                while(run){
                    System.out.println("Press 1 To Get Tasks In Given Date");
                    System.out.println("Press 2 To Get Tasks In Given Date For A Given Team");
                    System.out.println("Press 3 To Get Tasks For A Given Person");
                    System.out.println("Press 4 To Get Task In A Given Date For A Given Person");
                    System.out.println("Press 5 To Get A Team And Person In Given Task");
                    int ReportNumber = in.nextInt();

                    switch (ReportNumber) {
                        case 1 -> ConsoleOutputDisplay.GetTasksInDateX();
                        case 2 -> ConsoleOutputDisplay.GetTasksInDateXForTeamX();
                        case 3 -> ConsoleOutputDisplay.GetTasksForPersonX();
                        case 4 -> ConsoleOutputDisplay.GetTaskInDateXForPersonX();
                        default -> ConsoleOutputDisplay.GetTeamAndPeopleInTaskX();
                    }
                    System.out.println("Press 1 To Continue Or 0 To Quit");
                    ReportNumber = in.nextInt();
                    while (!in.hasNextInt()){
                        System.out.println("Press 1 To Continue Or 0 To Quit");
                        ReportNumber = in.nextInt();
                    }
                    if(ReportNumber == 0) run = false;

                }
            } else if (SetUpId == 2) {
                ConsoleInputDisplay.AddAnotherTask();
                System.out.println("Press 1 To Add Another Custom Person");
                SetUpId = in.nextInt();
                if(SetUpId == 1){
                    ConsoleInputDisplay.AddAnotherPerson();
                }//else {
                System.out.println("Press 3 To Add Another Custom Team");
                SetUpId = in.nextInt();
                if(SetUpId == 3){
                    ConsoleInputDisplay.AddAnotherTeam();
                }
                // }
                boolean runs = true;
                while (runs){
                    System.out.println("Press 1 To Update Team");
                    System.out.println("Press 2 To Delete Team");
                    System.out.println("Press 3 To Update Task");
                    System.out.println("Press 4 To Delete Task");
                    System.out.println("Press 5 To Update Person");
                    System.out.println("Press 6 To Delete Person");
                    System.out.println("Press 0 To Skip");
                    int Choice = in.nextInt();

                    switch (Choice) {
                        case 1 -> ConsoleInputDisplay.UpdateTeam();
                        case 2 -> ConsoleInputDisplay.DeleteTeam();
                        case 3 -> ConsoleInputDisplay.UpdateTask();
                        case 4 -> ConsoleInputDisplay.DeleteTask();
                        case 5 -> ConsoleInputDisplay.UpdatePerson();
                        case 6 -> ConsoleInputDisplay.DeletePerson();
                        default -> runs = false;
                    }

                }
                //TODO setup done, attaching begins here 2
                System.out.println("How Many Teams Do You Want To Attach People And Tasks?");
                int TeamCount = in.nextInt();
                while (TeamCount> 0){
                    ConsoleInputDisplay.AssignPeopleToTeams();
                    ConsoleInputDisplay.AssignTasksToTeams();
                    TeamCount --;
                }
                //TODO populate the calendar and Begin Reporting
                ConsoleInputDisplay.PopulateCalendar();
                //TODO REPORTING HERE
                boolean run = true;
                while(run){
                    System.out.println("Press 1 To Get Tasks In Given Date");
                    System.out.println("Press 2 To Get Tasks In Given Date For A Given Team");
                    System.out.println("Press 3 To Get Tasks For A Given Person");
                    System.out.println("Press 4 To Get Task In A Given Date For A Given Person");
                    System.out.println("Press 5 To Get A Team And Person In Given Task");
                    int ReportNumber = in.nextInt();

                    switch (ReportNumber) {
                        case 1 -> ConsoleOutputDisplay.GetTasksInDateX();
                        case 2 -> ConsoleOutputDisplay.GetTasksInDateXForTeamX();
                        case 3 -> ConsoleOutputDisplay.GetTasksForPersonX();
                        case 4 -> ConsoleOutputDisplay.GetTaskInDateXForPersonX();
                        default -> ConsoleOutputDisplay.GetTeamAndPeopleInTaskX();
                    }
                    System.out.println("Press 1 To Continue Or 0 To Quit");
                    ReportNumber = in.nextInt();
                    while (!in.hasNextInt()){
                        System.out.println("Press 1 To Continue Or 0 To Quit");
                        ReportNumber = in.nextInt();
                    }
                    run = ReportNumber == 1;

                }
            } else if (SetUpId == 3) {
                ConsoleInputDisplay.AddAnotherTeam();
                System.out.println("Press 1 To Add Another Custom Person");
                SetUpId = in.nextInt();
                if(SetUpId == 1){
                    ConsoleInputDisplay.AddAnotherPerson();
                }//else {
                System.out.println("Press 2 To Add Another Custom Task");
                SetUpId = in.nextInt();
                if(SetUpId == 2){
                    ConsoleInputDisplay.AddAnotherTask();
                }
                // }
                boolean runs = true;
                while (runs){
                    System.out.println("Press 1 To Update Team");
                    System.out.println("Press 2 To Delete Team");
                    System.out.println("Press 3 To Update Task");
                    System.out.println("Press 4 To Delete Task");
                    System.out.println("Press 5 To Update Person");
                    System.out.println("Press 6 To Delete Person");
                    System.out.println("Press 0 To Skip");
                    int Choice = in.nextInt();

                    switch (Choice) {
                        case 1 -> ConsoleInputDisplay.UpdateTeam();
                        case 2 -> ConsoleInputDisplay.DeleteTeam();
                        case 3 -> ConsoleInputDisplay.UpdateTask();
                        case 4 -> ConsoleInputDisplay.DeleteTask();
                        case 5 -> ConsoleInputDisplay.UpdatePerson();
                        case 6 -> ConsoleInputDisplay.DeletePerson();
                        default -> runs = false;
                    }

                }
                //TODO setup done, attaching begins here 3
                System.out.println("How Many Teams Do You Want To Attach People And Tasks?");
                int TeamCount = in.nextInt();
                while (TeamCount> 0){
                    ConsoleInputDisplay.AssignPeopleToTeams();
                    ConsoleInputDisplay.AssignTasksToTeams();
                    TeamCount --;
                }
                //TODO populate the calendar and Begin Reporting
                ConsoleInputDisplay.PopulateCalendar();
                //TODO REPORTING HERE
                boolean run = true;
                while(run){
                    System.out.println("Press 1 To Get Tasks In Given Date");
                    System.out.println("Press 2 To Get Tasks In Given Date For A Given Team");
                    System.out.println("Press 3 To Get Tasks For A Given Person");
                    System.out.println("Press 4 To Get Task In A Given Date For A Given Person");
                    System.out.println("Press 5 To Get A Team And Person In Given Task");
                    int ReportNumber = in.nextInt();

                    switch (ReportNumber) {
                        case 1 -> ConsoleOutputDisplay.GetTasksInDateX();
                        case 2 -> ConsoleOutputDisplay.GetTasksInDateXForTeamX();
                        case 3 -> ConsoleOutputDisplay.GetTasksForPersonX();
                        case 4 -> ConsoleOutputDisplay.GetTaskInDateXForPersonX();
                        default -> ConsoleOutputDisplay.GetTeamAndPeopleInTaskX();
                    }
                    System.out.println("Press 1 To Continue Or 0 To Quit");
                    ReportNumber = in.nextInt();
                    while (!in.hasNextInt()){
                        System.out.println("Press 1 To Continue Or 0 To Quit");
                        ReportNumber = in.nextInt();
                    }
                    run = ReportNumber ==1;

                }
            }
        }else {
            SetUpProject();
        }
    }

}
