package views;

import controllers.CalendarController;
import models.PersonModel;
import models.TaskModel;
import models.TeamModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import static controllers.PersonController.*;
import static controllers.TaskController.*;
import static controllers.TeamController.*;
import static helpers.FileAccessProcessor.readLinesToList;

public class ConsoleInputDisplay {
    //////////////////////CREATION////////////////////////////////////////////////////////////////////////////
    public static void CreatePeople() {
        System.out.println("How many people do you want to create?");
        Scanner in = new Scanner(System.in);
        if(!in.hasNextInt()){
            System.out.println("Enter valid number!! :( ");
            CreatePeople();
        }else {
           createPerson(in.nextInt());
            System.out.println("People created Successfully :)");
        }
    }
    public static void CreateTasks() {
        System.out.println("How many Tasks do you want to create?");
        Scanner in = new Scanner(System.in);
        if(!in.hasNextInt()){
            System.out.println("Enter valid number!! :( ");
            CreateTasks();
        }else {
            createTask(in.nextInt());
            System.out.println("Tasks created Successfully :)");
        }
    }
    public static void CreateTeams() {
        System.out.println("How many teams do you want to create?");
        Scanner in = new Scanner(System.in);
        if(!in.hasNextInt()){
            System.out.println("Enter valid number!! :(");
            CreateTeams();
        }else {
            createTeam(in.nextInt());
            System.out.println("Teams created Successfully :)");
        }
    }
    public static void AddAnotherTeam() {
        System.out.println("Enter Team Name");
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\n");
        String teamName = in.next();
        if(!teamName.equals("")){
            TeamModel t = new TeamModel();
            t.name = teamName;
            addATeam(t);
            System.out.println("Team added Successfully :)");
        }else{
            System.out.println("Type in valid team Name :(");
            AddAnotherTeam();
        }

    }
    public static void AddAnotherPerson() {
        System.out.println("Enter Person Name");
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\n");
        String personName = in.next();
        if(!personName.equals("")){
            PersonModel p = new PersonModel();
            p.name = personName;
            addAPerson(p);
            System.out.println("Person created Successfully :)");
        }else{
            System.out.println("Type in valid person Name :(");
            AddAnotherPerson();
        }
    }
    public static void AddAnotherTask() {
        System.out.println("Enter the name of the Task");
        Scanner in = new Scanner(System.in);
        in.useDelimiter("\n");
        String taskName = in.next();
        if(!taskName.equals("")){
            TaskModel t = new TaskModel();
            t.name = taskName;

            System.out.println("Enter Month Number for this Task");
            in.reset();
            int month = in.nextInt();
            while(month > 12){
                System.out.println("Enter Valid Month! :(");
                month = in.nextInt();
            }
            t.month = month;

            System.out.println("Enter Date of month for this Task");
            int day = in.nextInt();

            while(day > 28){
                System.out.println("Enter Valid Date for month! :(");
                day = in.nextInt();
            }

            t.dayOfMonth = day;

            t.setTaskDay();
            addATask(t);
            System.out.println("Task created Successfully :)");
        }else{
            System.out.println("Type in valid Task details :(");
            AddAnotherTask();
        }
    }
    ///////////////UPDATE OR DELETE/////////////////////////////
    public static void UpdateTeam() {
        Scanner in = new Scanner(System.in);
        DisplayAllTeams();
        System.out.println("Enter Team Id To Update");
        int TeamId = in.nextInt();
        TeamModel updateTeam = getTeamById(TeamId);
        if(updateTeam != null){
            System.out.println("Enter New Team Name");
            in.useDelimiter("\n");
            updateTeam.name = in.next();
        }
        while(updateTeam == null){
            System.out.println("Enter Correct Team Id To Update");
            TeamId = in.nextInt();
            updateTeam = getTeamById(TeamId);
        }
        updateTeamModels(updateTeam);
        System.out.println("Team Updated Successfully :)");
    }
    public static void DeleteTeam() {
        Scanner in = new Scanner(System.in);
        DisplayAllTeams();
        System.out.println("Enter Team Id To Delete");
        int TeamId = in.nextInt();
        TeamModel delTeam = getTeamById(TeamId);
        while(delTeam == null){
            System.out.println("Enter Correct Team Id To Delete");
            TeamId = in.nextInt();
            delTeam = getTeamById(TeamId);
        }
        deleteTeam(delTeam);
        System.out.println("Team Deleted Successfully :)");

    }
    public static void UpdateTask() {
        Scanner in = new Scanner(System.in);
        DisplayAllTasks();
        System.out.println("Enter Task Id To Update");
        int TaskId = in.nextInt();
        TaskModel updateTask = getTaskById(TaskId);
        if(updateTask != null){
            System.out.println("Enter New Task Name");
            in.useDelimiter("\n");
            updateTask.name = in.next();
        }
        while(updateTask == null){
            System.out.println("Enter Correct Task Id To Update");
            TaskId = in.nextInt();
            updateTask = getTaskById(TaskId);
        }
        updateTaskModels(updateTask);
        System.out.println("Task Updated Successfully :)");
    }
    public static void DeleteTask() {
        Scanner in = new Scanner(System.in);
        DisplayAllTasks();
        System.out.println("Enter Task Id To Delete");
        int TaskId = in.nextInt();
        TaskModel delTask = getTaskById(TaskId);
        while(delTask == null){
            System.out.println("Enter Correct Task Id To Delete");
            TaskId = in.nextInt();
            delTask = getTaskById(TaskId);
        }
        deleteTask(delTask);
        System.out.println("Task Deleted Successfully :)");
        //reload calendar Model on deletion
        PopulateCalendar();
    }
    public static void UpdatePerson() {
        Scanner in = new Scanner(System.in);
        DisplayAllPeople();
        System.out.println("Enter Peron Id To Update");
        int PersonId = in.nextInt();
        PersonModel updatePerson = getPersonById(PersonId);
        if(updatePerson != null){
            System.out.println("Enter New Person Name");
            in.useDelimiter("\n");
            updatePerson.name = in.next();
        }
        while(updatePerson == null){
            System.out.println("Enter Correct Person Id To Update");
            PersonId = in.nextInt();
            updatePerson = getPersonById(PersonId);
        }
        updatePersonModels(updatePerson);
        System.out.println("Person Updated Successfully :)");
    }
    public static void DeletePerson() {
        Scanner in = new Scanner(System.in);
        DisplayAllPeople();
        System.out.println("Enter Person Id To Delete");
        int PersonId = in.nextInt();
        PersonModel delPerson = getPersonById(PersonId);
        while(delPerson == null){
            System.out.println("Enter Correct Person Id To Delete");
            PersonId = in.nextInt();
            delPerson = getPersonById(PersonId);
        }
        deletePerson(delPerson);
        System.out.println("Person Deleted Successfully :)");
    }

    ////////////////RELATING////////////////////////////////////////////////////////////////
    public static void AssignPeopleToTeams() {
        AllPeopleAndTeams();
        PeopleAssignment();
    }
    public static void AssignTasksToTeams() {
        AllTasksAndTeams();
        TasksAssignment();
    }
    public static void DisplayAllTeams() {
        System.out.println("----------------");
        System.out.println("ID NAME   (TEAMS)");
        ArrayList<String> teams = readLinesToList(TeamModel.TEAMFILE);
        for (String team:teams) {
            String[] cols = team.split(",");
            System.out.println(cols[0] +" "+cols[1]);
        }
    }
    public static void PopulateCalendar() {
        CalendarController.loadCalendar();
    }
    public static void DisplayAllPeople() {
        System.out.println("----------------");
        System.out.println("ID NAME   (PEOPLE)");
        ArrayList<String> people = readLinesToList(PersonModel.PERSONFILE);
        for (String person:people) {
            String[] cols = person.split(",");
            System.out.println(cols[0] +" "+cols[1]);
        }
    }
    public static void DisplayAllTasks() {
        System.out.println("----------------");
        System.out.println("ID NAME   (TASKS)");
        ArrayList<String> tasks = readLinesToList(TaskModel.TASKFILE);
        for (String task:tasks) {
            String[] cols = task.split(",");
            System.out.println(cols[0] +" "+cols[1]);
        }
    }
    ////////////////////HELPERS/////////////////////////////////
    private static void TasksAssignment() {
        Scanner in = new Scanner(System.in);
        System.out.println("Choose a Team ID to attach tasks");
        int TeamId = in.nextInt();
        while (getTeamById(TeamId)  == null){
            System.out.println("Entered Team doesn't exist! :(");
            System.out.println("Choose another Team");
            TeamId = in.nextInt();
        }

        System.out.println("How many tasks to you wish to add to this team");
        //TODO check to make sure this count ain't bigger than people count
        int tasksCount = in.nextInt();
        int count = 0;
        ArrayList<Integer> TaskIds = new ArrayList<>();

        while (tasksCount > 0){
            if(count == 0) { System.out.println("Add First Task Id");}
            else {System.out.println("Add The Next Task Id");}
            TaskIds.add(in.nextInt());
            count++;
            tasksCount--;
        }
        //attach people to teams
        //get the team given the id
        TeamModel team = getTeamById(TeamId);
        //get all the people with these ids and add them to teammembers
        team.tasksAssigned = getTasksByIds(TaskIds);
        //update this team in file
        updateTeamModels(team);
    }
    private static void AllTasksAndTeams() {
        //read files to console
        ArrayList<String> tasks = readLinesToList(TaskModel.TASKFILE);
        System.out.println("ID NAME   (TASKS)");
        for (String task:tasks) {
            String[] cols = task.split(",");
            System.out.println(cols[0] +" "+cols[1]);
        }
        DisplayAllTeams();
    }
    private static void PeopleAssignment() {
        Scanner in = new Scanner(System.in);
        System.out.println("Choose a Team ID to attach people to");
        int TeamId = in.nextInt();

        System.out.println("How many distinct people to you wish to add to this team");
        int peopleCount = in.nextInt();//check to make sure this count ain't bigger than people count
        int count = 0;
        ArrayList<Integer> PersonIds = new ArrayList<>();

        while (peopleCount > 0){
            if(count == 0) { System.out.println("Add First Person Id");}
            else {System.out.println("Add The Next Person Id");}
            PersonIds.add(in.nextInt());
            count++;
            peopleCount--;
        }
        //attach people to teams
        //get the team given the id
        if(getTeamById(TeamId) == null){
            //TODO do something if team id ain't found
            System.out.println("Entered Team doesn't exist!");
            return;
        }else {
            TeamModel team = getTeamById(TeamId);
            //get all the people with these ids and add them to teammembers
            team.teamMembers = getPeopleByIds(PersonIds);
            //update this team in file
            updateTeamModels(team);

        }
    }
    private static void AllPeopleAndTeams() {
        //read files to console
        ArrayList<String> people = readLinesToList(PersonModel.PERSONFILE);
        System.out.println("ID NAME   (PEOPLE)");
        for (String person:people) {
            String[] cols = person.split(",");
            System.out.println(cols[0] +" "+cols[1]);
        }
        DisplayAllTeams();
    }
}
