package views;

import controllers.CalendarController;
import controllers.PersonController;
import controllers.TaskController;
import controllers.TeamController;
import models.PersonModel;
import models.TaskModel;
import models.TeamModel;

import java.util.ArrayList;
import java.util.Scanner;

public class ConsoleOutputDisplay {
    public static void GetTasksInDateX() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter Month Number For The Task To Find");
        int month = in.nextInt();
        while(month > 12){
            System.out.println("Enter Valid Month! :(");
            month = in.nextInt();
        }

        System.out.println("Enter Date of month for this Task");
        int day = in.nextInt();

        while(day > 28){
            System.out.println("Enter Valid Date for month! :(");
            day = in.nextInt();
        }
        ArrayList<TaskModel>tasks = TaskController.getTasksInMonthDay(month,day);
        if(tasks.isEmpty()){
            System.out.println("No Tasks Found for this MonthDay :(");
        }else {
            System.out.println("--TASK NAME--");
            for (TaskModel t:tasks) {
                System.out.println(t.name);
            }
        }
    }
    public static void GetTasksInDateXForTeamX() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter Month Number For The Task To Find");
        int month = in.nextInt();
        while(month > 12){
            System.out.println("Enter Valid Month! :(");
            month = in.nextInt();
        }

        System.out.println("Enter Date of month for this Task");
        int day = in.nextInt();

        while(day > 28){
            System.out.println("Enter Valid Date for month! :(");
            day = in.nextInt();
        }
        //display all teams
        ConsoleInputDisplay.DisplayAllTeams();

        System.out.println("Enter Team Id To Find");
        int TeamId = in.nextInt();

        TeamModel team = TeamController.getTeamById(TeamId);
        while (team == null){
            System.out.println("Enter Correct Team Id :(");
            TeamId = in.nextInt();

            team = TeamController.getTeamById(TeamId);
        }

        ArrayList<TaskModel>tasks = CalendarController.getTasksForTeamInMonthDay(team,month,day);
        if(tasks.isEmpty()){
            System.out.println("No Tasks For Team Found for this MonthDay :(");
        }else {
            System.out.println("--TASK NAME(S)--");
            for (TaskModel t:tasks) {
                System.out.println(t.name);
            }
        }
    }
    public static void GetTasksForPersonX() {
        Scanner in = new Scanner(System.in);
        ConsoleInputDisplay.DisplayAllPeople();

        System.out.println("Enter Person Id To Find");
        int PersonId = in.nextInt();

        PersonModel person = PersonController.getPersonById(PersonId);
        while (person == null){
            System.out.println("Enter Correct Team Id :(");
            PersonId = in.nextInt();

            person = PersonController.getPersonById(PersonId);
        }
        ArrayList<TaskModel>tasks = TeamController.getTasksForPerson(person);
        if(tasks.isEmpty()){
            System.out.println("No Tasks For Team Found for this MonthDay :(");
        }else {
            System.out.println("--TASK NAME(S)-- For "+person.name);
            for (TaskModel t:tasks) {
                System.out.println(t.name);
            }
        }
    }
    public static void GetTaskInDateXForPersonX() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter Month Number For The Task To Find");
        int month = in.nextInt();
        while(month > 12){
            System.out.println("Enter Valid Month! :(");
            month = in.nextInt();
        }

        System.out.println("Enter Date of month for this Task");
        int day = in.nextInt();

        while(day > 28){
            System.out.println("Enter Valid Date for month! :(");
            day = in.nextInt();
        }
        //display all teams
        ConsoleInputDisplay.DisplayAllPeople();

        System.out.println("Enter Person Id To Find");
        int PersonId = in.nextInt();

        PersonModel person = PersonController.getPersonById(PersonId);
        while (person == null){
            System.out.println("Enter Correct Person Id :(");
            PersonId = in.nextInt();

            person = PersonController.getPersonById(PersonId);
        }

        ArrayList<TaskModel>tasks = CalendarController.getTasksForPersonInMonthDay(person,month,day);
        if(tasks.isEmpty()){
            System.out.println("No Tasks For "+person.name +" Found for this MonthDay :(");
        }else {
            System.out.println("--TASK NAME(S)-- For "+person.name);
            for (TaskModel t : tasks) {
                System.out.println(t.name);
            }
        }
    }
    public static void GetTeamAndPeopleInTaskX() {
        Scanner in = new Scanner(System.in);
        ConsoleInputDisplay.DisplayAllTasks();

        System.out.println("Enter Task Id To Find");
        int TaskId = in.nextInt();

        TaskModel task = TaskController.getTaskById(TaskId);
        while (task == null){
            System.out.println("Enter Correct Task Id :(");
            TaskId = in.nextInt();

            task = TaskController.getTaskById(TaskId);
        }
        ArrayList<TeamModel>teams = TeamController.getTeamsHavingTask(task);
        if(teams.isEmpty()){
            System.out.println("No Teams And People Have This Task Attached :(");
        }else {
            System.out.println("--TEAM AND MEMBER NAME(S)--");
            System.out.println();

            for (TeamModel t:teams) {
                System.out.println("Team Name(s)");
                System.out.println(t.name);
                System.out.println();

                if(t.teamMembers != null){
                    System.out.println("Member Name(s)");
                    for (PersonModel p:t.teamMembers) {
                        System.out.println(p.name);
                    };
                }
            }
        }

    }
}
