public class FileAccess {

//    public FileAccess(){}
//
//
//
//    public static String fullFilePath(String filename){
//        return "/home/jayced1loved/Documents/"+filename;
//    }
//
//    public static ArrayList<String> loadFile(String fullFilePath) throws IOException {
//        ArrayList<String> lines = new ArrayList<>();
//        File file = new File(fullFilePath(fullFilePath));
//
//        file.createNewFile();  //creates new file
//
//        Scanner output = new Scanner(file);
//        while (output.hasNext()){
//            lines.add(output.nextLine());
//        }
//        return lines;
//    }
//    //convert to respective models
//    public static ArrayList<PersonModel>convertToPersonModels(ArrayList<String>lines){
//        //id,name,tid
//        ArrayList<PersonModel>output = new ArrayList<>();
//
//        for (String line:lines) {
//            String[] cols = line.split(",");
//            PersonModel p = new PersonModel();
//            p.id = Integer.parseInt(cols[0]);
//            p.name = cols[1];
//            //p.teamId = Integer.parseInt(cols[2]);
//            output.add(p);
//        }
//        return output;
//    }
//    //save to respective model files
//    public static void saveToPersonFile( PersonModel p,String fullFilePath) throws IOException {
//        //id,name //receives data
//        ArrayList<String>lines = new ArrayList<>();
//        ArrayList<PersonModel>personModels  = convertToPersonModels(loadFile(fullFilePath));
//        File file = new File(fullFilePath(fullFilePath));
//        FileWriter myWriter = new FileWriter(file);
//
//        int currentId = 1;
//        if(personModels.size() >= 1){
//            //add the current person  with id of old + 1
//            currentId = max(personModels.stream().map(PersonModel::getId).toList()) + 1;
//        }
//        p.id = currentId;
//        personModels.add(p);
//        for (PersonModel person :personModels) {
//      //      lines.add("" + person.id + "," + person.name +"," +p.teamId + System.lineSeparator() + "");
//        }
//        for (String line:lines ) {
//            myWriter.write(line);
//        }
//        myWriter.close();
//    }
//    public static ArrayList<TeamModel>convertToTeamModels(ArrayList<String>lines) throws IOException {
//        //id,name,pids|pids|pids,tids^tids^tids
//        ArrayList<TeamModel>output = new ArrayList<>();
//        ArrayList<PersonModel> personModels  = convertToPersonModels(loadFile(PERSONFILE));
//        ArrayList<TaskModel> taskModels  = convertToTaskModels(loadFile(TASKFILE));
//        for (String line:lines) {
//            String[] cols = line.split(",");
//            TeamModel t = new TeamModel();
//            t.id = Integer.parseInt(cols[0]);
//            t.name = cols[1];
//
//            String[] personIds = cols[2].split("\\|");
//            for (String id:personIds ) {
//                t.teamMembers.add(personModels.stream().filter(x -> x.getId() == Integer.parseInt(id)).findFirst().orElse(null));
//            }
//
//            String[] taskIds = cols[3].split("\\^");
//            for (String id:taskIds) {
//                t.tasksAssigned.add(taskModels.stream().filter(x -> x.getId()==Integer.parseInt(id)).findFirst().orElse(null));
//            }
//            output.add(t);
//        }
//        return output;
//    }
//    public static void saveTeamToFile(TeamModel t, String fullFilePath) throws IOException {
//        //id,name,pids|pids|pids,tids^tids^tids
//        ArrayList<String>lines = new ArrayList<>();
//        ArrayList<TeamModel>teamModels  = convertToTeamModels(loadFile(fullFilePath));
//        File file = new File(fullFilePath(fullFilePath));
//        FileWriter myWriter = new FileWriter(file);
//
//        int currentId = 1;
//        if(teamModels.size() >= 1){
//            currentId = max(teamModels.stream().map(TeamModel::getId).toList()) + 1;
//        }
//        t.id = currentId;
//        teamModels.add(t);
//
//        for (TeamModel tm:teamModels) {
//            lines.add(""+tm.id+","+tm.name+","+ convertModelToString(tm.teamMembers,"|")+","
//                    +convertModelToString(tm.tasksAssigned,"^") + System.lineSeparator() + "");
//        }
//
//        for (String line:lines ) {
//            myWriter.write(line);
//        }
//        myWriter.close();
//    }
//    private static <T extends Model> String convertModelToString(ArrayList<T> models, String delimiter){
//        StringBuilder output = new StringBuilder();
//        if(models.size() == 0){
//            return output.toString();
//        }
//        for (T model:models) {
//            output.append(model.getId()).append(delimiter);
//        }
//        output = new StringBuilder(output.substring(0, output.length() - 1));
//        return output.toString();
//    }
//    private static ArrayList<TaskModel> convertToTaskModels(ArrayList<String> lines) {
//        //id,name,teamId,month|day{Calendarday}
//        ArrayList<TaskModel> output = new ArrayList<>();
//        for (String line:lines) {
//            String[] cols = line.split(",");
//            TaskModel t = new TaskModel();
//            t.id = Integer.parseInt(cols[0]);
//            t.name = cols[1];
//            String[] monthDay = cols[2].split("|");
//            t.taskDay = MonthDay.of(t.month = Integer.parseInt(monthDay[0]),t.dayOfMonth = Integer.parseInt(monthDay[1]));
//
//            output.add(t);
//        }
//        return output;
//    }
//    public static void saveTaskToFile(TaskModel t,String fullFilePath) throws IOException {
//        //id,name,teamId,month|day{Calendarday}
//        ArrayList<String>lines = new ArrayList<>();
//        ArrayList<TaskModel>taskModels  = convertToTaskModels(loadFile(fullFilePath));
//        File file = new File(fullFilePath(fullFilePath));
//        FileWriter myWriter = new FileWriter(file);
//
//        int currentId = 1;
//        if(taskModels.size()>=1){
//            currentId = max(taskModels.stream().map(TaskModel::getId).toList()) + 1;
//        }
//        t.id = currentId;
//        taskModels.add(t);
//
//        for (TaskModel tm:taskModels) {
//            lines.add(""+ tm.id + "," + tm.name + "," + tm.month + "|" + tm.dayOfMonth + System.lineSeparator() + "");
//        }
//
//        for (String line:lines ) {
//            myWriter.write(line);
//        }
//        myWriter.close();
//
//    }
//    private static ArrayList<TaskModel> convertToTCalendarModels(){
//        //Tasks are attached to a monthDay
//        //teams have tasked attached to them.
//        return null;
//    }
//    private static void updatePeopleModels(PersonModel person, TeamModel team) throws IOException {
//        ArrayList<PersonModel>people = FileAccessProcessor.convertStringToPersonModel(FileAccessProcessor.readLinesToList(FileAccess.PERSONFILE));
//
//
//        PersonModel oldPerson = new PersonModel();
//        for (PersonModel p:people) {
//            if(p.id == person.id){
//                oldPerson = p;
//            }
//        }
//        people.remove(oldPerson);
//        //person.team = team;//delete
//        people.add(person);
////        models.PersonModel oldman = new models.PersonModel();
////        people.forEach(x -> x.getId() == );
////        people.forEach(models.PersonModel -> {
////            int id = models.PersonModel.id;
////            if(teamMembers.stream().anyMatch(models.PersonModel))
////        });
////
////        for (Iterator<models.PersonModel> it = people.iterator(); it.hasNext();) {
////            for(Iterator<models.PersonModel> newman = teamMembers.iterator(); newman.hasNext();){
////                if (it.next().id == newman.next().id) {
////                    System.out.println("removed id " +it.next().id+"");
////                    System.out.print(System.in);
////                    people.remove(it.next());
////                    people.add(newman.next());
////
////                }
////            }
//
//
////        }
////        for (models.PersonModel p:people) {
////            for (models.PersonModel newPerson:teamMembers) {
////                if(p.id == newPerson.id){
////                    newPerson.team = team;
////                    people.remove(p);
////                    people.add(newPerson);
////                    System.out.println("Id " +p.id+" found");
////                    break;
////                }
////            }
////        }
////        savePeopleToFileFirstTime(people);
//    }


}
